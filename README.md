A ToDo Task Manager

Features:

1. You can add new task with Task Title, description and deadline date.

2. You can edit tasks, provided the task has not been marked as done, by clicking on task title.
After clicking the task title, its details will auto populate over the form where you can edit and submit them.

3. In case you have selected a task and want to add a new task, you can click on clear button which will clear the form for 
new tasks to be added.

4. By Clicking on the Icon button of each task, you can mark the task as done.

5. If the task is over due, its content will be shown in red.