import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:todo_task_manager/models/RegularTask.dart';
import 'package:todo_task_manager/models/ScheduledTask.dart';
import 'package:todo_task_manager/models/Task.dart';
import 'package:todo_task_manager/providers/HomeScreenViewModel.dart';
import 'package:todo_task_manager/utils/AppUtil.dart';
import 'package:todo_task_manager/utils/Constants.dart';
import 'package:todo_task_manager/utils/NotificationSetUp.dart';
import 'package:todo_task_manager/widgets/AddEditRegularTaskForm.dart';
import 'package:todo_task_manager/widgets/RegularTaskTile.dart';
import 'package:todo_task_manager/widgets/ScheduledTaskTile.dart';

class HomeScreen extends StatefulWidget {
  final String title;

  HomeScreen({this.title});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeScreenViewModel homeScreenViewModel;

  @override
  void initState() {
    homeScreenViewModel =
        Provider.of<HomeScreenViewModel>(context, listen: false);
    homeScreenViewModel.setHomeScreenViewModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _totalHeight = MediaQuery.of(context).size.height;
    double _totalWidth = MediaQuery.of(context).size.width;

    Widget _getTaskTiles(HomeScreenViewModel homeScreenViewModel) {
      if (homeScreenViewModel.allRegularTasks.length == 0 &&
          homeScreenViewModel.allScheduledTasks.length == 0) {
        return Center(
          child: Text('No task found. Please add some'),
        );
      }
      List<Widget> _childrenWidgets = [];

      if (homeScreenViewModel.allScheduledTasks.length > 0) {
        _childrenWidgets.add(Container(
          width: _totalWidth,
          padding: const EdgeInsets.only(bottom: 8, top: 12),
          child: Row(
            children: [
              Expanded(
                child: Divider(
                  color: Colors.grey,
                  indent: 20,
                  endIndent: 10,
                ),
              ),
              Text(
                'Your Scheduled Task List',
                style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey),
              ),
              Expanded(
                child: Divider(
                  color: Colors.grey,
                  indent: 10,
                  endIndent: 20,
                ),
              ),
            ],
          ),
        ));

        _childrenWidgets.add(Expanded(
          child: ListView.builder(
            itemCount: homeScreenViewModel.allScheduledTasks.length,
            itemBuilder: (context, index) {
              ScheduledTask task =
                  homeScreenViewModel.allScheduledTasks.elementAt(index);

              return ScheduledTaskTile(
                scheduledTask: task,
              );
            },
          ),
        ));
      }

      if (homeScreenViewModel.allRegularTasks.length > 0) {
        _childrenWidgets.add(Container(
          width: _totalWidth,
          padding: const EdgeInsets.only(bottom: 8, top: 12),
          child: Row(
            children: [
              Expanded(
                child: Divider(
                  color: Colors.grey,
                  indent: 20,
                  endIndent: 10,
                ),
              ),
              Text(
                'Your Task List',
                style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey),
              ),
              Expanded(
                child: Divider(
                  color: Colors.grey,
                  indent: 10,
                  endIndent: 20,
                ),
              ),
            ],
          ),
        ));

        _childrenWidgets.add(Expanded(
          child: ListView.builder(
            itemCount: homeScreenViewModel.allRegularTasks.length,
            itemBuilder: (context, index) {
              Task task = homeScreenViewModel.allRegularTasks.elementAt(index);
              bool taskOverDue = AppUtil.checkIfTaskOverDue(task);

              return RegularTaskTile(
                task: task,
                taskOverDue: taskOverDue,
              );
            },
          ),
        ));
      }

      return Column(
        mainAxisSize: MainAxisSize.min,
        children: _childrenWidgets,
      );
    }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: AppUtil.mainScreenScaffoldKey,
      floatingActionButton: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FloatingActionButton(
            backgroundColor: mainThemeColor,
            child: Icon(
              Icons.add,
              color: Colors.black,
            ),
            onPressed: () {
              AppUtil.showBottomForm(RegularTask());
            },
          ),
          SizedBox(
            height: 10,
          ),
          FloatingActionButton(
            backgroundColor: mainThemeColor,
            child: Icon(
              Icons.repeat,
              color: Colors.black,
            ),
            onPressed: () {
              AppUtil.showBottomForm(ScheduledTask());
            },
          )
        ],
      ),
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: mainThemeColor,
      ),
      body: Consumer<HomeScreenViewModel>(
        builder: (context, homeScreenViewModel, child) {
          return Column(
            children: [
              Expanded(
                child: homeScreenViewModel.allTaskSet
                    ? _getTaskTiles(homeScreenViewModel)
                    : Center(
                        child: CircularProgressIndicator(),
                      ),
              ),
            ],
          );
        },
      ),
    );
  }
}
