import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_task_manager/models/ScheduledTask.dart';
import 'package:todo_task_manager/providers/HomeScreenViewModel.dart';
import 'package:todo_task_manager/utils/AppUtil.dart';
import 'package:todo_task_manager/utils/Constants.dart';

class ScheduledTaskTile extends StatelessWidget {
  final ScheduledTask scheduledTask;

  ScheduledTaskTile({
    this.scheduledTask,
  });

  @override
  Widget build(BuildContext context) {
    double _totalWidth = MediaQuery.of(context).size.width;
    HomeScreenViewModel homeScreenViewModel =
        Provider.of<HomeScreenViewModel>(context, listen: false);

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Container(
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            boxShadow: [
              BoxShadow(
                color: taskTileShadowColor,
                blurRadius: 1,
                spreadRadius: 1,
                offset: Offset(
                  1.2,
                  1.2,
                ),
              ),
            ],
            color: mainThemeColor),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: _totalWidth,
              child: Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        // if task is not marked as done only then allow user to select and edit the task
                        homeScreenViewModel
                            .setCurrentScheduledTask(scheduledTask);
                        AppUtil.showBottomForm(scheduledTask);
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                        child: Row(
                          children: [
                            Expanded(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 4.0),
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(scheduledTask.taskTitle,
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.black)),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: Text(scheduledTask.description,
                                        style: TextStyle(color: Colors.black)),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 4.0),
                        child: Text(
                          ScheduledTask.convertDayOfWeekToJson(
                              scheduledTask.day_of_week),
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                      Text(
                        AppUtil.displayTimeOfDay(
                            AppUtil.convertTimeOfDayFromString(
                                scheduledTask.timeOfDay)),
                        style: TextStyle(color: Colors.black),
                      ),
                    ],
                  ),
                  IconButton(
                      padding: const EdgeInsets.only(left: 10),
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        Provider.of<HomeScreenViewModel>(context, listen: false)
                            .deleteScheduledTask(scheduledTask, context);
                      })
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
