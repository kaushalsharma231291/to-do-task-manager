import 'package:flutter/material.dart';
import 'package:todo_task_manager/models/ScheduledTask.dart';
import 'package:todo_task_manager/utils/Constants.dart';

class DayOfWeekPickerDialog extends StatefulWidget {
  @override
  _DayOfWeekPickerDialogState createState() => _DayOfWeekPickerDialogState();
}

class _DayOfWeekPickerDialogState extends State<DayOfWeekPickerDialog> {
  Day_Of_Week _choosenDay;

  List<Widget> _getDaysTiles() {
    List<Widget> tiles = [];
    Day_Of_Week.values.forEach((element) {
      bool selected = false;

      if (_choosenDay != null && _choosenDay == element) {
        selected = true;
      }

      tiles.add(InkWell(
        onTap: () {
          setState(() {
            if (selected) {
              _choosenDay = null;
            } else {
              _choosenDay = element;
            }
          });
        },
        child: Container(
          height: 60,
          width: MediaQuery.of(context).size.width,
          child: Row(
            children: [
              Expanded(child: SizedBox()),
              Text(
                ScheduledTask.convertDayOfWeekToJson(element),
                style: TextStyle(
                  color: selected ? mainThemeColor : Colors.grey,
                  fontSize: selected ? 16 : 14,
                ),
              ),
              Expanded(child: SizedBox()),
            ],
          ),
        ),
      ));
    });
    return tiles;
  }

  Widget _getSubmitButton() {
    return GestureDetector(
      onTap: () {
        if (_choosenDay != null) {
          Navigator.of(context).pop(_choosenDay);
        }
      },
      child: Container(
        height: 70,
        width: 100,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
          color: _choosenDay != null ? mainThemeColor : Colors.grey,
        ),
        child: Center(
          child: Text(
            'Submit',
            style: TextStyle(
                fontWeight:
                    _choosenDay != null ? FontWeight.bold : FontWeight.normal),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _childrenWidgets = [];
    _childrenWidgets.add(
      Text(
        'Choose Day of week',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );

    _childrenWidgets.addAll(_getDaysTiles());

    _childrenWidgets.add(_getSubmitButton());
    return SimpleDialog(
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: _childrenWidgets,
        )
      ],
    );
  }
}
