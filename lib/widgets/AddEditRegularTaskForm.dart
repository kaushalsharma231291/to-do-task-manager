import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:todo_task_manager/providers/HomeScreenViewModel.dart';
import 'package:todo_task_manager/utils/Constants.dart';

class AddEditRegularTaskForm extends StatefulWidget {
  @override
  _AddEditRegularTaskFormState createState() => _AddEditRegularTaskFormState();
}

class _AddEditRegularTaskFormState extends State<AddEditRegularTaskForm> {
  final GlobalKey<FormState> _addEditRegularFormKey = GlobalKey<FormState>();

  void _onSubmitPressed(HomeScreenViewModel homeScreenViewModel) {
    if (_addEditRegularFormKey.currentState.validate() &&
        homeScreenViewModel.deadline != null) {
      homeScreenViewModel.addEditRegularTask(context);
    } else {
      setState(() {
        homeScreenViewModel.autoValidateRegularForm = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    HomeScreenViewModel homeScreenViewModel =
        Provider.of<HomeScreenViewModel>(context, listen: false);
    return Container(
      child: Form(
        key: _addEditRegularFormKey,
        autovalidate: homeScreenViewModel.autoValidateRegularForm,
        child: Container(
          margin: const EdgeInsets.all(12),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: InkWell(
                  onTap: () {
                    showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime.now(),
                      lastDate: DateTime.now().add(Duration(days: 730)),
                    ).then((deadlineDate) {
                      if (deadlineDate != null) {
                        showTimePicker(
                                context: context, initialTime: TimeOfDay.now())
                            .then((deadlineTime) {
                          if (deadlineTime != null) {
                            setState(() {
                              DateTime deadline = DateTime(
                                deadlineDate.year,
                                deadlineDate.month,
                                deadlineDate.day,
                                deadlineTime.hour,
                                deadlineTime.minute,
                              );
                              homeScreenViewModel.deadline =
                                  deadline.toIso8601String();
                            });
                          }
                        });
                      }
                    });
                  },
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    child: homeScreenViewModel.deadline == null
                        ? Center(
                            child: Text(
                            'Please click here and choose deadline',
                            style: TextStyle(color: Colors.red),
                          ))
                        : Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Text('DeadLine'),
                              ),
                              Expanded(
                                child: SizedBox(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Text(DateFormat.yMMMEd().add_jm().format(
                                    DateTime.parse(
                                        homeScreenViewModel.deadline))),
                              )
                            ],
                          ),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: TextFormField(
                  controller: homeScreenViewModel.regularTaskTitleController,
                  decoration: InputDecoration(
                      labelText: 'Task Title', hintText: 'Ex : Do HomeWork'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Required';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: TextFormField(
                  controller: homeScreenViewModel.regularDescriptionController,
                  decoration: InputDecoration(
                      labelText: 'Description',
                      hintText: 'Ex : Start doing homework'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Required';
                    }
                    return null;
                  },
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        color: mainThemeColor,
                        onPressed: () => _onSubmitPressed(homeScreenViewModel),
                        child: Text('Submit',
                            style: TextStyle(color: Colors.black)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        color: mainThemeColor,
                        onPressed: homeScreenViewModel.clearCurrentRegularTask,
                        child: Text('Clear',
                            style: TextStyle(color: Colors.black)),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
