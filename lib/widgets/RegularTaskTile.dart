import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:todo_task_manager/models/RegularTask.dart';
import 'package:todo_task_manager/models/Task.dart';
import 'package:todo_task_manager/providers/HomeScreenViewModel.dart';
import 'package:todo_task_manager/utils/AppUtil.dart';
import 'package:todo_task_manager/utils/Constants.dart';

class RegularTaskTile extends StatelessWidget {
  final RegularTask task;
  final bool taskOverDue;

  RegularTaskTile({this.task, this.taskOverDue});

  @override
  Widget build(BuildContext context) {
    double _totalWidth = MediaQuery.of(context).size.width;
    HomeScreenViewModel homeScreenViewModel =
        Provider.of<HomeScreenViewModel>(context, listen: false);

    void _onTaskIconPressed(RegularTask task) {
      if (!task.taskDone) {
        homeScreenViewModel.markRegularTaskDone(task, context);
      }
    }

    Icon _getTaskIcon(RegularTask task) {
      if (task.taskDone) {
        // if task is marked as done
        return Icon(
          Icons.check_circle,
          color: Colors.green,
        );
      }

      if (AppUtil.checkIfTaskOverDue(task)) {
        // if task is not marked done and is over due as well
        return Icon(
          Icons.announcement,
          color: overDueRedColor,
        );
      }

      return Icon(
        // if task is neither marked done nor is over due
        Icons.check_circle,
        color: Colors.grey[350],
      );
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Container(
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            boxShadow: [
              BoxShadow(
                color: taskTileShadowColor,
                blurRadius: 1,
                spreadRadius: 1,
                offset: Offset(
                  1.2,
                  1.2,
                ),
              ),
            ],
            color: mainThemeColor),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: _totalWidth,
              child: Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        if (!task.taskDone) {
                          // if task is not marked as done only then allow user to select and edit the task
                          homeScreenViewModel.setCurrentRegularTask(task);
                          AppUtil.showBottomForm(task);
                        }
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                        child: Row(
                          children: [
                            Expanded(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 4.0),
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(task.taskTitle,
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w500,
                                              color: taskOverDue
                                                  ? overDueRedColor
                                                  : Colors.black)),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: Text(task.description,
                                        style: TextStyle(
                                            color: taskOverDue
                                                ? overDueRedColor
                                                : Colors.black)),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 4.0),
                        child: Text(
                          DateFormat.E().format(
                            DateTime.parse(task.deadline),
                          ),
                          style: TextStyle(
                              color:
                                  taskOverDue ? overDueRedColor : Colors.black),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 4.0),
                        child: Text(
                          DateFormat('d-MMM-yy').format(
                            DateTime.parse(task.deadline),
                          ),
                          style: TextStyle(
                              color:
                                  taskOverDue ? overDueRedColor : Colors.black),
                        ),
                      ),
                      Text(
                        DateFormat.jm().format(
                          DateTime.parse(task.deadline),
                        ),
                        style: TextStyle(
                            color:
                                taskOverDue ? overDueRedColor : Colors.black),
                      ),
                    ],
                  ),
                  IconButton(
                    onPressed: () => _onTaskIconPressed(task),
                    icon: _getTaskIcon(task),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
