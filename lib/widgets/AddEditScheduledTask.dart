import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:todo_task_manager/models/ScheduledTask.dart';
import 'package:todo_task_manager/providers/HomeScreenViewModel.dart';
import 'package:todo_task_manager/utils/AppUtil.dart';
import 'package:todo_task_manager/utils/Constants.dart';

class AddEditScheduledTaskForm extends StatefulWidget {
  @override
  _AddEditScheduledTaskFormState createState() =>
      _AddEditScheduledTaskFormState();
}

class _AddEditScheduledTaskFormState extends State<AddEditScheduledTaskForm> {
  final GlobalKey<FormState> _addEditScheduledFormKey = GlobalKey<FormState>();

  void _onSubmitPressed(HomeScreenViewModel homeScreenViewModel) {
    if (_addEditScheduledFormKey.currentState.validate()) {
      homeScreenViewModel.addEditScheduledTask(context);
    } else {
      setState(() {
        homeScreenViewModel.autoValidateScheduledForm = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    HomeScreenViewModel homeScreenViewModel =
        Provider.of<HomeScreenViewModel>(context, listen: false);
    return Container(
      child: Form(
        key: _addEditScheduledFormKey,
        autovalidate: homeScreenViewModel.autoValidateScheduledForm,
        child: Container(
          margin: const EdgeInsets.all(12),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: InkWell(
                  onTap: () {
                    AppUtil.showDayOfWeekPicker(context).then((selectedDay) {
                      if (selectedDay != null) {
                        showTimePicker(
                                context: context, initialTime: TimeOfDay.now())
                            .then((selectedTime) {
                          if (selectedTime != null) {
                            setState(() {
                              homeScreenViewModel.setDayAndTime(
                                  selectedDay, selectedTime);
                            });
                          }
                        });
                      }
                    });
                  },
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    child: homeScreenViewModel.timeOfDay == null
                        ? Center(
                            child: Text(
                            'Please click here and choose schedule',
                            style: TextStyle(color: Colors.red),
                          ))
                        : Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Text('Schedule'),
                              ),
                              Expanded(
                                child: SizedBox(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text('Every ' +
                                        ScheduledTask.convertDayOfWeekToJson(
                                            homeScreenViewModel.day_of_week)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(AppUtil.displayTimeOfDay(
                                        homeScreenViewModel.timeOfDay)),
                                  ],
                                ),
                              )
                            ],
                          ),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: TextFormField(
                  controller: homeScreenViewModel.scheduledTaskTitleController,
                  decoration: InputDecoration(
                      labelText: 'Task Title', hintText: 'Ex : Do HomeWork'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Required';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: TextFormField(
                  controller:
                      homeScreenViewModel.scheduledDescriptionController,
                  decoration: InputDecoration(
                      labelText: 'Description',
                      hintText: 'Ex : Start doing homework'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Required';
                    }
                    return null;
                  },
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        color: mainThemeColor,
                        onPressed: () => _onSubmitPressed(homeScreenViewModel),
                        child: Text('Submit',
                            style: TextStyle(color: Colors.black)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        color: mainThemeColor,
                        onPressed:
                            homeScreenViewModel.clearCurrentScheduledTask,
                        child: Text('Clear',
                            style: TextStyle(color: Colors.black)),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
