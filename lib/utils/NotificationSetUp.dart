import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:todo_task_manager/models/RegularTask.dart';
import 'package:todo_task_manager/models/ScheduledTask.dart';
import 'package:todo_task_manager/models/Task.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:todo_task_manager/utils/AppUtil.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
const NotificationDetails notificationDetails = const NotificationDetails(
    android: AndroidNotificationDetails(
  'your channel id',
  'your channel name',
  'your channel description',
  sound: RawResourceAndroidNotificationSound('wake_up'),
));

const MethodChannel platform =
    MethodChannel('dexterx.dev/flutter_local_notifications_example');

class NotificationSetUp {
  Future<void> setUpPushNotifications() async {
    tz.initializeTimeZones();
    tz.setLocalLocation(tz.local);
    final NotificationAppLaunchDetails notificationAppLaunchDetails =
        await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('notification_icon');

    /// Note: permissions aren't requested here just to demonstrate that can be
    /// done later
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            requestAlertPermission: false,
            requestBadgePermission: false,
            requestSoundPermission: false,
            onDidReceiveLocalNotification:
                (int id, String title, String body, String payload) async {});
    const MacOSInitializationSettings initializationSettingsMacOS =
        MacOSInitializationSettings(
            requestAlertPermission: false,
            requestBadgePermission: false,
            requestSoundPermission: false);
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS,
            macOS: initializationSettingsMacOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {});
  }

  static Future<void> scheduleNotification(RegularTask task) async {
    tz.TZDateTime tzDateTime = tz.TZDateTime.parse(tz.local, task.deadline);
    await flutterLocalNotificationsPlugin.zonedSchedule(task.id, task.taskTitle,
        task.description, tzDateTime, notificationDetails,
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime);
  }

  static Future<void> scheduleNotificationForScheduledTask(
      ScheduledTask task) async {
    List<DateTime> scheduleList = AppUtil.getDateTimeListForWeekDay(
        task.day_of_week,
        AppUtil.convertTimeOfDayFromString(task.timeOfDay),
        DateTime.parse(task.createdOn));
    int i = 0;
    for (DateTime dateTime in scheduleList) {
      tz.TZDateTime tzDateTime =
          tz.TZDateTime.parse(tz.local, dateTime.toIso8601String());
      await flutterLocalNotificationsPlugin.zonedSchedule(task.id + i,
          task.taskTitle, task.description, tzDateTime, notificationDetails,
          androidAllowWhileIdle: true,
          uiLocalNotificationDateInterpretation:
              UILocalNotificationDateInterpretation.absoluteTime);
      i++;
    }
  }

  static Future<void> cancelNotificationForScheduledTask(
      ScheduledTask task) async {
    List<DateTime> scheduleList = AppUtil.getDateTimeListForWeekDay(
        task.day_of_week,
        AppUtil.convertTimeOfDayFromString(task.timeOfDay),
        DateTime.parse(task.createdOn));
    int i = 0;
    for (DateTime dateTime in scheduleList) {
      await flutterLocalNotificationsPlugin.cancel(task.id + i);
      i++;
    }
  }

  static Future<void> cancelNotification(Task task) async {
    await flutterLocalNotificationsPlugin.cancel(task.id);
  }
}
