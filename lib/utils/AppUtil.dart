import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:todo_task_manager/models/RegularTask.dart';
import 'package:todo_task_manager/models/ScheduledTask.dart';
import 'package:todo_task_manager/models/Task.dart';
import 'package:todo_task_manager/widgets/AddEditRegularTaskForm.dart';
import 'package:todo_task_manager/widgets/AddEditScheduledTask.dart';
import 'package:todo_task_manager/widgets/DayOfWeekPickerDialog.dart';

class AppUtil {
  static final mainScreenScaffoldKey = GlobalKey<ScaffoldState>();
  static showLoadingDailog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text('Please wait'),
          children: <Widget>[
            Center(
              child: CircularProgressIndicator(),
            ),
          ],
        );
      },
    );
  }

  static bool checkIfTaskOverDue(RegularTask task) {
    if (task.taskDone) {
      return false;
    }
    DateTime deadline = DateTime.parse(task.deadline);
    DateTime now = DateTime.now();
    if (now.isAfter(deadline)) {
      return true;
    }
    return false;
  }

  static void showBottomForm(Task task) {
    mainScreenScaffoldKey.currentState.showBottomSheet(
      (ctx) {
        return Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(-3, 3),
                    spreadRadius: 5,
                    blurRadius: 5,
                  )
                ],
                color: Colors.white),
            child: task.runtimeType.toString() == 'RegularTask'
                ? AddEditRegularTaskForm()
                : AddEditScheduledTaskForm());
      },
      elevation: 1,
      backgroundColor: Colors.white,
    );
  }

  static void closeBottomSheet() {
    Navigator.of(mainScreenScaffoldKey.currentState.context).pop();
  }

  static List<DateTime> getDateTimeListForWeekDay(
      Day_Of_Week day_of_week, TimeOfDay timeOfDay, DateTime createdOn) {
    List<DateTime> returnList = [];
    int requiredDay = convertDayIntoInt(day_of_week);
    // first find the nearest date to day of week
    while (createdOn.weekday != requiredDay) {
      createdOn = createdOn.add(Duration(days: 1));
    }

    returnList.add(getDateTimeAccordingToTimeOfDay(createdOn, timeOfDay));

    int i = 1;
    while (i < 52) {
      createdOn = createdOn.add(Duration(days: 7));
      returnList.add(getDateTimeAccordingToTimeOfDay(createdOn, timeOfDay));
      i++;
    }
    return returnList;
  }

  static int convertDayIntoInt(Day_Of_Week day_of_week) {
    switch (day_of_week) {
      case Day_Of_Week.Monday:
        return 1;
      case Day_Of_Week.Tuesday:
        return 2;
      case Day_Of_Week.Wednesday:
        return 3;
      case Day_Of_Week.Thursday:
        return 4;
      case Day_Of_Week.Friday:
        return 5;
      case Day_Of_Week.Saturday:
        return 6;
      case Day_Of_Week.Sunday:
        return 7;
    }
  }

  static DateTime getDateTimeAccordingToTimeOfDay(
      DateTime dateTime, TimeOfDay timeOfDay) {
    return DateTime(dateTime.year, dateTime.month, dateTime.day, timeOfDay.hour,
        timeOfDay.minute);
  }

  static TimeOfDay convertTimeOfDayFromString(String timeOfDay) {
    return TimeOfDay(
        hour: int.parse(timeOfDay.split(':').first),
        minute: int.parse(timeOfDay.split(':').last));
  }

  static String displayTimeOfDay(TimeOfDay timeOfDay) {
    final now = new DateTime.now();
    final dt = DateTime(
        now.year, now.month, now.day, timeOfDay.hour, timeOfDay.minute);
    final format = DateFormat.jm(); //"6:00 AM"
    return format.format(dt);
  }

  static Future<Day_Of_Week> showDayOfWeekPicker(BuildContext context) {
    return showDialog<Day_Of_Week>(
      context: context,
      builder: (context) {
        return DayOfWeekPickerDialog();
      },
    );
  }
}
