import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;
import 'package:todo_task_manager/models/RegularTask.dart';
import 'package:todo_task_manager/models/ScheduledTask.dart';
import 'package:todo_task_manager/models/Task.dart';

class DatabaseHelper {
  static Future<sql.Database> _getSqlDatabase() async {
    final databasePath = await sql.getDatabasesPath();
    return sql
        .openDatabase(path.join(databasePath, 'todoTaskManagerDatabase.db'),
            onCreate: (database, version) async {
      await database.execute(
          "CREATE TABLE RegularTask(id INT PRIMARY KEY, taskTitle TEXT, description TEXT, deadline TEXT, taskDone INT)");
      await database.execute(
          "CREATE TABLE ScheduledTask(id INT PRIMARY KEY, taskTitle TEXT, description TEXT, timeOfDay TEXT, day_of_week TEXT, createdOn TEXT)");
    }, version: 1);
  }

  static Future<void> insertRegularTask(RegularTask task) async {
    final sqlDatabase = await _getSqlDatabase();
    await sqlDatabase.insert(
      'RegularTask',
      task.toJson(),
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<List<RegularTask>> getAllRegularTasks() async {
    final sqlDatabase = await _getSqlDatabase();
    List<RegularTask> taskList = [];
    List<Map<String, Object>> tableEntries =
        await sqlDatabase.query('RegularTask');
    tableEntries.forEach((entry) {
      RegularTask task = RegularTask.fromJson(entry);
      taskList.add(task);
    });
    return taskList;
  }

  static Future<List<ScheduledTask>> getAllScheduledTasks() async {
    final sqlDatabase = await _getSqlDatabase();
    List<ScheduledTask> taskList = [];
    List<Map<String, Object>> tableEntries =
        await sqlDatabase.query('ScheduledTask');
    tableEntries.forEach((entry) {
      ScheduledTask task = ScheduledTask.fromJson(entry);
      taskList.add(task);
    });
    return taskList;
  }

  static Future<void> insertScheduledTask(ScheduledTask task) async {
    final sqlDatabase = await _getSqlDatabase();
    await sqlDatabase.insert(
      'ScheduledTask',
      task.toJson(),
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<void> deleteScheduledTask(ScheduledTask task) async {
    final sqlDatabase = await _getSqlDatabase();
    await sqlDatabase
        .rawQuery('DELETE FROM ScheduledTask WHERE id=?', [task.id]);
  }
}
