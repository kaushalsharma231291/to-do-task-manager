import 'package:flutter/material.dart';

Color taskTileShadowColor = const Color.fromRGBO(124, 125, 156, 1);

Color mainThemeColor = const Color.fromRGBO(202, 204, 252, 1);

Color overDueRedColor = const Color.fromRGBO(130, 20, 31, 1);
