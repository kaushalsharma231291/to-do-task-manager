import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_task_manager/HomeScreen.dart';
import 'package:todo_task_manager/providers/HomeScreenViewModel.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => HomeScreenViewModel(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Task Manager',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomeScreen(title: 'Task Manager'),
      ),
    );
  }
}
