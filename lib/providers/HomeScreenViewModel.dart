import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_task_manager/models/RegularTask.dart';
import 'package:todo_task_manager/models/ScheduledTask.dart';
import 'package:todo_task_manager/models/Task.dart';
import 'package:todo_task_manager/utils/AppUtil.dart';
import 'package:todo_task_manager/utils/DatabaseHelper.dart';
import 'package:todo_task_manager/utils/NotificationSetUp.dart';

class HomeScreenViewModel with ChangeNotifier {
  RegularTask _currentRegularTask;
  List<RegularTask> _allRegularTasks;
  ScheduledTask _currentScheduledTask;
  List<ScheduledTask> _allScheduledTasks;
  String deadline;
  TextEditingController regularTaskTitleController;
  TextEditingController regularDescriptionController;
  TextEditingController scheduledTaskTitleController;
  TextEditingController scheduledDescriptionController;
  bool autoValidateRegularForm = false;
  bool autoValidateScheduledForm = false;
  bool allTaskSet =
      false; // this flag will be used to mark if some task are being updated in DB or if all tasks are set and can be displayed

  Day_Of_Week _day_of_week;
  TimeOfDay _timeOfDay;

  Day_Of_Week get day_of_week {
    return this._day_of_week;
  }

  TimeOfDay get timeOfDay {
    return this._timeOfDay;
  }

  List<RegularTask> get allRegularTasks {
    return [...this._allRegularTasks];
  }

  RegularTask get currentRegularTask {
    return this._currentRegularTask;
  }

  List<ScheduledTask> get allScheduledTasks {
    return [...this._allScheduledTasks];
  }

  ScheduledTask get currentScheduledTask {
    return this._currentScheduledTask;
  }

  void setDayAndTime(Day_Of_Week day_of_week, TimeOfDay timeOfDay) {
    this._day_of_week = day_of_week;
    this._timeOfDay = timeOfDay;
  }

  void sortAllRegularTask() {
    if (this._allRegularTasks.length > 0) {
      this._allRegularTasks.sort((t1, t2) {
        DateTime t1Deadline = DateTime.parse(t1.deadline);
        DateTime t2Deadline = DateTime.parse(t2.deadline);
        if (t1Deadline.isAfter(t2Deadline)) {
          return 1;
        }
        return -1;
      });
    }
  }

  Future<void> setHomeScreenViewModel() async {
    // this function will be called during initialization phase of home screen
    //and will set all variables and components of this view model

    this._allRegularTasks = await DatabaseHelper.getAllRegularTasks();
    this.sortAllRegularTask();
    this._allScheduledTasks = await DatabaseHelper.getAllScheduledTasks();
    this._currentRegularTask = RegularTask(id: Random().nextInt(1000));
    this.regularTaskTitleController =
        TextEditingController(text: _currentRegularTask.taskTitle);
    this.regularDescriptionController =
        TextEditingController(text: _currentRegularTask.description);
    this.deadline = _currentRegularTask.deadline;

    this._currentScheduledTask = ScheduledTask(id: Random().nextInt(1000));
    this.scheduledTaskTitleController =
        TextEditingController(text: _currentScheduledTask.taskTitle);
    this.scheduledDescriptionController =
        TextEditingController(text: _currentScheduledTask.description);
    this.allTaskSet = true;
    notifyListeners();
  }

  void clearCurrentRegularTask() {
    // this function will be used to clear the add edit form in case some task has been selected
    // and user wants to clear the details and add new task

    this._currentRegularTask = RegularTask(id: Random().nextInt(1000));
    this.regularTaskTitleController.clear();
    this.deadline = null;
    this.regularDescriptionController.clear();
    notifyListeners();
  }

  void clearCurrentScheduledTask() {
    // this function will be used to clear the add edit form in case some task has been selected
    // and user wants to clear the details and add new task

    this._currentScheduledTask = ScheduledTask(id: Random().nextInt(1000));
    this.scheduledTaskTitleController.clear();
    this.scheduledDescriptionController.clear();
    notifyListeners();
  }

  void setCurrentRegularTask(RegularTask task) {
    // this function will set the selected task as current task so that
    // the details of the selected task can be view and edited in the form

    this._currentRegularTask = task;
    this.regularTaskTitleController.value =
        TextEditingValue(text: task.taskTitle);
    this.regularDescriptionController.value =
        TextEditingValue(text: task.description);
    this.deadline = task.deadline;
    this.autoValidateRegularForm = false;
    notifyListeners();
  }

  void setCurrentScheduledTask(ScheduledTask task) {
    // this function will set the selected task as current task so that
    // the details of the selected task can be view and edited in the form

    this._currentScheduledTask = task;
    this.scheduledTaskTitleController.value =
        TextEditingValue(text: task.taskTitle);
    this.scheduledDescriptionController.value =
        TextEditingValue(text: task.description);
    this._day_of_week = task.day_of_week;
    this._timeOfDay = TimeOfDay(
        hour: int.parse(task.timeOfDay.split(':').first),
        minute: int.parse(task.timeOfDay.split(':').last));
    this.autoValidateScheduledForm = false;
    notifyListeners();
  }

  Future<void> markRegularTaskDone(
      RegularTask task, BuildContext context) async {
    // this function will be called on clicking the Icon Button of task title
    // to mark this task as done

    AppUtil.showLoadingDailog(context);
    RegularTask newTask = RegularTask(
        id: task.id,
        taskTitle: task.taskTitle,
        deadline: task.deadline,
        description: task.description,
        taskDone: true);
    await DatabaseHelper.insertRegularTask(newTask);
    await NotificationSetUp.cancelNotification(newTask);
    Navigator.of(context).pop();
    this.allTaskSet = false;
    notifyListeners();
    this._allRegularTasks = await DatabaseHelper.getAllRegularTasks();
    this.sortAllRegularTask();
    this.allTaskSet = true;
    notifyListeners();
  }

  Future<void> addEditRegularTask(BuildContext context) async {
    // called on clicking submit button of add/edit form
    // this function will add new task into DB, in case current task was selected
    // from the list of all task then this would update the details in DB

    bool editingTask = false;
    if (this._currentRegularTask.deadline != null) {
      editingTask = true;
    }

    AppUtil.showLoadingDailog(context);

    this._currentRegularTask = RegularTask(
      id: this._currentRegularTask.id,
      deadline: this.deadline,
      description: this.regularDescriptionController.text,
      taskDone: this._currentRegularTask.taskDone ?? false,
      taskTitle: this.regularTaskTitleController.text,
    );

    await DatabaseHelper.insertRegularTask(this._currentRegularTask);

    if (editingTask) {
      // if we are editing task, first cancel the previously scheduled notification
      await NotificationSetUp.cancelNotificationForScheduledTask(
          this._currentScheduledTask);

      // then schedule another notification with the updated deadline
    }

    await NotificationSetUp.scheduleNotification(this._currentRegularTask);

    this.autoValidateRegularForm = false;
    this.deadline = null;
    this.regularDescriptionController.clear();
    this.regularTaskTitleController.clear();
    this._currentRegularTask = RegularTask(id: Random().nextInt(1000));
    AppUtil.closeBottomSheet();
    Navigator.of(context).pop();
    this.allTaskSet = false;
    notifyListeners();
    this._allRegularTasks = await DatabaseHelper.getAllRegularTasks();
    this.sortAllRegularTask();
    this.allTaskSet = true;
    notifyListeners();
  }

  Future<void> addEditScheduledTask(BuildContext context) async {
    // called on clicking submit button of add/edit form
    // this function will add new task into DB, in case current task was selected
    // from the list of all task then this would update the details in DB

    bool editingTask = false;
    if (this._currentScheduledTask.day_of_week != null) {
      editingTask = true;
    }

    AppUtil.showLoadingDailog(context);

    if (editingTask) {
      // if we are editing task, first cancel the previously scheduled notification
      await NotificationSetUp.cancelNotificationForScheduledTask(
          this._currentScheduledTask);

      // then schedule another notification with the updated deadline
    }

    this._currentScheduledTask = ScheduledTask(
      id: this._currentScheduledTask.id,
      description: this.scheduledDescriptionController.text,
      taskTitle: this.scheduledTaskTitleController.text,
      createdOn: DateTime.now().toIso8601String(),
      day_of_week: this._day_of_week,
      timeOfDay: this._timeOfDay.hour.toString() +
          ':' +
          this._timeOfDay.minute.toString(),
    );

    await DatabaseHelper.insertScheduledTask(this._currentScheduledTask);

    await NotificationSetUp.scheduleNotificationForScheduledTask(
        this._currentScheduledTask);

    this.autoValidateScheduledForm = false;
    this.scheduledDescriptionController.clear();
    this.scheduledTaskTitleController.clear();
    this._currentScheduledTask = ScheduledTask(id: Random().nextInt(1000));
    AppUtil.closeBottomSheet();
    Navigator.of(context).pop();
    this.allTaskSet = false;
    notifyListeners();
    this._allScheduledTasks = await DatabaseHelper.getAllScheduledTasks();
    this.allTaskSet = true;
    notifyListeners();
  }

  Future<void> deleteScheduledTask(
      ScheduledTask scheduledTask, BuildContext context) async {
    AppUtil.showLoadingDailog(context);

    await NotificationSetUp.cancelNotificationForScheduledTask(scheduledTask);

    await DatabaseHelper.deleteScheduledTask(scheduledTask);

    this.autoValidateScheduledForm = false;
    this.scheduledDescriptionController.clear();
    this.scheduledTaskTitleController.clear();
    this._currentScheduledTask = ScheduledTask(id: Random().nextInt(1000));
    Navigator.of(context).pop();
    this.allTaskSet = false;
    notifyListeners();
    this._allScheduledTasks = await DatabaseHelper.getAllScheduledTasks();
    this.allTaskSet = true;
    notifyListeners();
  }
}
