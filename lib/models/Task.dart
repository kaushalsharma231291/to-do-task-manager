import 'package:json_annotation/json_annotation.dart';

part 'Task.g.dart';

@JsonSerializable()
class Task {
  final int id;
  final String taskTitle;
  final String description;

  Task({
    this.id,
    this.taskTitle,
    this.description,
  });

  factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);

  Map<String, dynamic> toJson() => _$TaskToJson(this);

  static bool convertBoolFromJson(int value) {
    if (value == 0) {
      return false;
    } else {
      return true;
    }
  }

  static int convertBoolToJson(bool value) {
    if (value) {
      return 1;
    } else {
      return 0;
    }
  }
}
