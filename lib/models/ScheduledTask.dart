import 'package:json_annotation/json_annotation.dart';
import 'package:todo_task_manager/models/Task.dart';

part 'ScheduledTask.g.dart';

enum Day_Of_Week {
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
  Sunday
}

@JsonSerializable()
class ScheduledTask extends Task {
  final String timeOfDay;
  @JsonKey(
      fromJson: ScheduledTask.convertDayOfWeekFromJson,
      toJson: ScheduledTask.convertDayOfWeekToJson)
  final Day_Of_Week day_of_week;
  final String createdOn;

  ScheduledTask({
    int id,
    String taskTitle,
    String description,
    this.timeOfDay,
    this.day_of_week,
    this.createdOn,
  }) : super(id: id, taskTitle: taskTitle, description: description);

  factory ScheduledTask.fromJson(Map<String, dynamic> json) =>
      _$ScheduledTaskFromJson(json);

  Map<String, dynamic> toJson() => _$ScheduledTaskToJson(this);

  static Day_Of_Week convertDayOfWeekFromJson(String value) {
    switch (value) {
      case 'Monday':
        return Day_Of_Week.Monday;
      case 'Tuesday':
        return Day_Of_Week.Tuesday;
      case 'Wednesday':
        return Day_Of_Week.Wednesday;
      case 'Thursday':
        return Day_Of_Week.Thursday;
      case 'Friday':
        return Day_Of_Week.Friday;
      case 'Saturday':
        return Day_Of_Week.Saturday;
      case 'Sunday':
        return Day_Of_Week.Sunday;
      default:
        throw Exception('convertDayOfWeekFromJson exception: No value found');
    }
  }

  static String convertDayOfWeekToJson(Day_Of_Week day_of_week) {
    switch (day_of_week) {
      case Day_Of_Week.Monday:
        return 'Monday';
      case Day_Of_Week.Tuesday:
        return 'Tuesday';
      case Day_Of_Week.Wednesday:
        return 'Wednesday';
      case Day_Of_Week.Thursday:
        return 'Thursday';
      case Day_Of_Week.Friday:
        return 'Friday';
      case Day_Of_Week.Saturday:
        return 'Saturday';
      case Day_Of_Week.Sunday:
        return 'Sunday';
      default:
        throw Exception('convertDayOfWeekToJson exception: No value found');
    }
  }
}
