import 'package:json_annotation/json_annotation.dart';
import 'package:todo_task_manager/models/Task.dart';

part 'RegularTask.g.dart';

@JsonSerializable()
class RegularTask extends Task {
  final String deadline;
  @JsonKey(fromJson: Task.convertBoolFromJson, toJson: Task.convertBoolToJson)
  final bool taskDone;

  RegularTask({
    int id,
    String taskTitle,
    String description,
    this.deadline,
    this.taskDone,
  }) : super(id: id, taskTitle: taskTitle, description: description);

  factory RegularTask.fromJson(Map<String, dynamic> json) =>
      _$RegularTaskFromJson(json);

  Map<String, dynamic> toJson() => _$RegularTaskToJson(this);
}
