// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RegularTask.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegularTask _$RegularTaskFromJson(Map<String, dynamic> json) {
  return RegularTask(
    id: json['id'] as int,
    taskTitle: json['taskTitle'] as String,
    description: json['description'] as String,
    deadline: json['deadline'] as String,
    taskDone: Task.convertBoolFromJson(json['taskDone'] as int),
  );
}

Map<String, dynamic> _$RegularTaskToJson(RegularTask instance) =>
    <String, dynamic>{
      'id': instance.id,
      'taskTitle': instance.taskTitle,
      'description': instance.description,
      'deadline': instance.deadline,
      'taskDone': Task.convertBoolToJson(instance.taskDone),
    };
