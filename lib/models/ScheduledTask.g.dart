// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ScheduledTask.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScheduledTask _$ScheduledTaskFromJson(Map<String, dynamic> json) {
  return ScheduledTask(
    id: json['id'] as int,
    taskTitle: json['taskTitle'] as String,
    description: json['description'] as String,
    timeOfDay: json['timeOfDay'] as String,
    day_of_week:
        ScheduledTask.convertDayOfWeekFromJson(json['day_of_week'] as String),
    createdOn: json['createdOn'] as String,
  );
}

Map<String, dynamic> _$ScheduledTaskToJson(ScheduledTask instance) =>
    <String, dynamic>{
      'id': instance.id,
      'taskTitle': instance.taskTitle,
      'description': instance.description,
      'timeOfDay': instance.timeOfDay,
      'day_of_week': ScheduledTask.convertDayOfWeekToJson(instance.day_of_week),
      'createdOn': instance.createdOn,
    };
