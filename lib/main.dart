import 'package:flutter/material.dart';
import 'package:todo_task_manager/App.dart';
import 'package:todo_task_manager/utils/NotificationSetUp.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await NotificationSetUp().setUpPushNotifications();
  runApp(App());
}
